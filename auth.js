// Dependencies Import
const jwt = require("jsonwebtoken");
const secret = "EcommerceApi";


//Retrieve Access Token
module.exports.createAccessToken = (user) => {

	const data = {

		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin

	}

	return jwt.sign(data, secret, {});

}

//Verify a User
module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization;


	if (typeof token === "undefined"){

		return res.send({auth: "Failed. Invalid/No Token."})

	} else {

		token = token.slice(7, token.length);


		jwt.verify(token, secret, function(error, decodedToken){

			if (error) {

				return res.send({
					auth: "Failed",
					message: error.message
				})

			} else {

				req.user = decodedToken;
				next();

			}
		})

	}
}

//Verify if User Admin
module.exports.verifyAdmin = (req, res, next) => {

	if (req.user.isAdmin){
		
		next();

	} else {

		return res.send({
			auth: "Failed",
			message: "Action Forbidden"
		})
	}

}