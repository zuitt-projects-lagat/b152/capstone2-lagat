//Order Model Import
const Order = require("../models/Order");

//Product Model Import
const Product = require("../models/Product");

// auth Module Import
const auth = require("../auth");



//Checkout or Create an Order
module.exports.createOrder = (req,res) => {

	//console.log(req.user.id);


	if (req.user.isAdmin){

		res.send("This feature is for Non-Admin Users only.");

	} else {

		//console.log(req.body);

		let newOrder = new Order ({
		
			userId: req.user.id,
			totalAmount: req.body.totalAmount,
			products: req.body.products

		})

		newOrder.save();


		for (let i = 0; i < req.body.products.length; i++){

			Product.findById(req.body.products[i].productId)
			.then(product =>{

				let order = {
					orderId: newOrder.id,
					quantity: req.body.products[i].quantity
				}

				product.orders.push(order);

				return product.save();			

			})
		}


		res.send("You have successfully created an order!");

	}

}

//Retrieve every User's Orders
module.exports.getUserOrders = (req,res) => {

	//console.log(req.user.id);

	if (req.user.isAdmin){

		res.send("This feature is for Non-Admin Users only.");

	} else {

		Order.find({userId: req.user.id})
		.then(result => res.send(result))
		.catch(error => res.send(error));

	}

}

//Retrieve All Orders
module.exports.getAllOrders = (req,res) => {

	//console.log(req.user);

	Order.find({})
	.then(result => res.send(result))
	.catch(error => res.send(error));

}

//Display Products Per Order
module.exports.productsPerOrder = (req,res) => {

	//console.log(req.params.id);

	if (req.user.isAdmin) {

		res.send("This feature is for Non-Admin Users only.");

	} else {

		Order.findById(req.params.id)
		.then(order =>{

			//console.log(order.userId);
			//console.log(req.user.id);

			if (order.userId !== req.user.id){
				res.send("Access Denied! Order was made by another user.");
			} else {
				res.send(order.products);
			}
		})
		.catch(error => res.send(error));

	}
}