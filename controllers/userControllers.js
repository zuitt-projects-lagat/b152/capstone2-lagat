//User Model Import
const User = require("../models/User");

// bcrypt Import
const bcrypt = require("bcrypt");

// auth Module Import
const auth = require("../auth");



//Register a User
module.exports.registerUser = (req,res) => {

	//console.log(req.body);

	//Encrypting User Password
	const hashedPW = bcrypt.hashSync(req.body.password, 10);

	let newUser = new User ({

		firstName: req.body.firstName,
		lastName: req.body.lastName,
		email: req.body.email,
		password: hashedPW,
		mobileNo: req.body.mobileNo

	})

	newUser.save()
	.then(user => res.send(user))
	.catch(error => res.send(error));

}

//Login a User
module.exports.loginUser = (req,res) => {

	//console.log(req.body);

	User.findOne({email: req.body.email})
	.then(foundUser => {
		if (foundUser === null) {
			res.send("No User Found.")
		} else {

			const isPasswordCorrect = bcrypt.compareSync(req.body.password, foundUser.password);
			//console.log(isPasswordCorrect);

			if (isPasswordCorrect){
				return res.send({accessToken: auth.createAccessToken(foundUser)});
			} else {
				return res.send("Password is incorrect.")
			}
		}
	})
	.catch(error => res.send(error));

}

//Set User as an Admin
module.exports.updateAdmin = (req,res) => {

	//console.log(req.params.id);

	User.findByIdAndUpdate(req.params.id, {isAdmin: true}, {new: true})
	.then(updatedUser => res.send(updatedUser))
	.catch(error => res.send(error));

}