//Product Model Import
const Product = require("../models/Product");

// auth Module Import
const auth = require("../auth");



//Adding a Product to the Product List
module.exports.addProduct = (req,res) => {

	console.log(req.body);

	let newProduct = new Product ({

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	})

	newProduct.save()
	.then(product => res.send(product))
	.catch(error => res.send(error));

}

//Retrieve all Active Products
module.exports.getActiveProducts = (req,res) => {

	Product.find({isActive: true})
	.then(activeProducts => res.send(activeProducts))
	.catch(error => res.send(error));

}

//Retrieve a Single Product
module.exports.getSingleProduct = (req,res) => {

	//console.log(req.params.id)

	Product.findById(req.params.id)
	.then(product => res.send(product))
	.catch(error => res.send(error));

}

//Update a Product
module.exports.updateProduct = (req,res) => {

	console.log(req.params.id)

	let updates = {

		name: req.body.name,
		description: req.body.description,
		price: req.body.price

	}

	Product.findByIdAndUpdate(req.params.id, updates, {new: true})
	.then(updatedCourse => res.send(updatedCourse))
	.catch(error => res.send(error));

}

//Archive a Product
module.exports.archiveProduct = (req,res) => {

	//console.log(req.params.id);

	Product.findByIdAndUpdate(req.params.id, {isActive: false}, {new: true})
	.then(product => res.send(product))
	.catch(error => res.send(error));

}

//Activate a Product
module.exports.activateProduct = (req,res) => {

	//console.log(req.params.id);

	Product.findByIdAndUpdate(req.params.id, {isActive: true}, {new: true})
	.then(product => res.send(product))
	.catch(error => res.send(error));

}