type: E-commerce API

User
firstName - string
lastName - string
emails - string
password - string
mobileNo - string
isAdmin - boolean
		  default: false

Product
name - string
description - string
price - number
isActive - boolean
			default: true
createdOn - date
			default: new Date
orders: [

	{
		orderId
		quantity
	}

]

associative entity - two way embed

{
	orderId
	productId
	quantity
}

Order
userID - string
totalAmount - number
purchaseOn - date
			default: new Date
product: [

	{
		productId
		quantity
	}

]