const express = require("express");
const mongoose = require("mongoose");
const app = express();
const port = 4000;

//API to MongoDb Connection
mongoose.connect("mongodb+srv://gnikkolagat:1234512345@cluster0.klkgh.mongodb.net/ecommerceAPI152?retryWrites=true&w=majority",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	});


//Notifications
let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("Connected to MongoDB"));

app.use(express.json());


//User Route Import
const userRoutes = require('./routes/userRoutes');
//Endpoint Assignment for User Routes
app.use('/users', userRoutes);

//Product Route Import
const productRoutes = require('./routes/productRoutes');
//Endpoint Assignment for Product Routes
app.use('/products', productRoutes);

//Order Route Import
const orderRoutes = require('./routes/orderRoutes');
//Endpoint Assignment for Order Routes
app.use('/orders', orderRoutes);


app.listen(port, () => console.log(`Server is running at localhost:4000`));