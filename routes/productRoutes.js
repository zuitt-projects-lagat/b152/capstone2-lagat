const express = require("express");
const router = express.Router();

//Product Controllers Import
const productControllers = require("../controllers/productControllers");

// auth Module Import
const auth = require("../auth");

//Middleware Destructuring
const {verify, verifyAdmin} = auth;



//Add Product Listing
router.post('/', verify, verifyAdmin, productControllers.addProduct);

//Get all Active Products
router.get('/active', productControllers.getActiveProducts);

//Get a Single Product
router.get('/getSingleProduct/:id', productControllers.getSingleProduct);

//Update a Product
router.put('/:id', verify, verifyAdmin, productControllers.updateProduct);

//Archive a Product
router.put('/archive/:id', verify, verifyAdmin, productControllers.archiveProduct);

//Activate a Product
router.put('/activate/:id', verify, verifyAdmin, productControllers.activateProduct);



module.exports = router;