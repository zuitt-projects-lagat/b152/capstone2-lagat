const express = require("express");
const router = express.Router();

//User Controllers Import
const userControllers = require("../controllers/userControllers");

// auth Module Import
const auth = require("../auth");

//Middleware Destructuring
const {verify, verifyAdmin} = auth;



//User Registration
router.post('/', userControllers.registerUser);

//User Authentication
router.post('/login', userControllers.loginUser);

//Set User to Admin
router.put('/updateAdmin/:id', verify, verifyAdmin, userControllers.updateAdmin);



module.exports = router;