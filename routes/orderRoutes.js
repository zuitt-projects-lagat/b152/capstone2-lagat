const express = require("express");
const router = express.Router();

//Order Controllers Import
const orderControllers = require("../controllers/orderControllers")

// auth Module Import
const auth = require("../auth");

//Middleware Destructuring
const {verify, verifyAdmin} = auth;



//Create an Order
router.post('/', verify, orderControllers.createOrder);

//Retrieve a User's Orders
router.get('/getUserOrders', verify, orderControllers.getUserOrders);

//Retrieve All Orders
router.get('/', verify, verifyAdmin, orderControllers.getAllOrders);

//Display Products Per Order
router.get('/displayProducts/:id', verify, orderControllers.productsPerOrder);



module.exports = router;